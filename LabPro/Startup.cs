﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LabPro.Startup))]
namespace LabPro
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
