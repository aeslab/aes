﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LabPro.Models;

namespace LabPro.Controllers
{
    public class PruebasController : Controller
    {
        private DB_EPPEntities db = new DB_EPPEntities();

        // GET: Pruebas
        public async Task<ActionResult> Index()
        {
            return View(await db.pruebas.ToListAsync());
        }

        // GET: Pruebas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pruebas pruebas = await db.pruebas.FindAsync(id);
            if (pruebas == null)
            {
                return HttpNotFound();
            }
            return View(pruebas);
        }

        // GET: Pruebas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pruebas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,nombre,norma,descripcion")] pruebas pruebas)
        {
            if (ModelState.IsValid)
            {
                db.pruebas.Add(pruebas);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(pruebas);
        }

        // GET: Pruebas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pruebas pruebas = await db.pruebas.FindAsync(id);
            if (pruebas == null)
            {
                return HttpNotFound();
            }
            return View(pruebas);
        }

        // POST: Pruebas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,nombre,norma,descripcion")] pruebas pruebas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pruebas).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(pruebas);
        }

        // GET: Pruebas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pruebas pruebas = await db.pruebas.FindAsync(id);
            if (pruebas == null)
            {
                return HttpNotFound();
            }
            return View(pruebas);
        }

        // POST: Pruebas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            pruebas pruebas = await db.pruebas.FindAsync(id);
            db.pruebas.Remove(pruebas);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
