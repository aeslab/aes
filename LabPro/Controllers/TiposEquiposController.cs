﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LabPro.Models;

namespace LabPro.Controllers
{
    public class TiposEquiposController : Controller
    {
        private DB_EPPEntities db = new DB_EPPEntities();

        // GET: TiposEquipos
        public async Task<ActionResult> Index()
        {
            return View(await db.tipos_equipo.ToListAsync());
        }

        // GET: TiposEquipos/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipos_equipo tipos_equipo = await db.tipos_equipo.FindAsync(id);
            if (tipos_equipo == null)
            {
                return HttpNotFound();
            }
            return View(tipos_equipo);
        }

        // GET: TiposEquipos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TiposEquipos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,nombre,nomenclatura")] tipos_equipo tipos_equipo)
        {
            if (ModelState.IsValid)
            {
                db.tipos_equipo.Add(tipos_equipo);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(tipos_equipo);
        }

        // GET: TiposEquipos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipos_equipo tipos_equipo = await db.tipos_equipo.FindAsync(id);
            if (tipos_equipo == null)
            {
                return HttpNotFound();
            }
            return View(tipos_equipo);
        }

        // POST: TiposEquipos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,nombre,nomenclatura")] tipos_equipo tipos_equipo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipos_equipo).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(tipos_equipo);
        }

        // GET: TiposEquipos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipos_equipo tipos_equipo = await db.tipos_equipo.FindAsync(id);
            if (tipos_equipo == null)
            {
                return HttpNotFound();
            }
            return View(tipos_equipo);
        }

        // POST: TiposEquipos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            tipos_equipo tipos_equipo = await db.tipos_equipo.FindAsync(id);
            db.tipos_equipo.Remove(tipos_equipo);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
