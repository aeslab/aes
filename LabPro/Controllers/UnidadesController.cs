﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LabPro.Models;

namespace LabPro.Controllers
{
    public class UnidadesController : Controller
    {
        private DB_EPPEntities db = new DB_EPPEntities();

        // GET: Unidades
        public async Task<ActionResult> Index()
        {
            var query = await (from u in db.unidad
                        join d in db.departamentos on u.departamentosId equals d.Id
                        join e in db.empresas on d.empresasId equals e.Id
                        select new  {u.Id, u.nombre, Departamento = d.nombre, Empresa = e.nombre }).ToListAsync();
            var unidades = query.Select(x => new unidad { Id = x.Id, nombre = x.nombre, Departamento = x.Departamento, Empresa = x.Empresa }).ToList();
            return View(unidades);
        }

        // GET: Unidades/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            unidad unidad = await db.unidad.FindAsync(id);
            if (unidad == null)
            {
                return HttpNotFound();
            }
            return View(unidad);
        }

        // GET: Unidades/Create
        public ActionResult Create()
        {
            ViewBag.empresas = new SelectList(db.empresas, "Id", "nombre");
            return View();
        }

        // POST: Unidades/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,nombre,departamentosId")] unidad unidad)
        {
            if (ModelState.IsValid)
            {
                db.unidad.Add(unidad);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.departamentosId = new SelectList(db.departamentos, "Id", "nombre", unidad.departamentosId);

            return View(unidad);
        }

        // GET: Unidades/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            unidad unidad = await db.unidad.FindAsync(id);
            if (unidad == null)
            {
                return HttpNotFound();
            }
            ViewBag.departamentosId = new SelectList(db.departamentos.Where(x=> x.empresasId == unidad.departamentos.empresas.Id),"Id","nombre",unidad.departamentos.Id);
            ViewBag.empresasId = new SelectList(db.empresas, "Id", "nombre", unidad.departamentos.empresas.Id);
            return View(unidad);
        }

        // POST: Unidades/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,nombre,departamentosId,empleados_Id")] unidad unidad)
        {
            if (ModelState.IsValid)
            {
                db.Entry(unidad).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.departamentosId = new SelectList(db.departamentos, "Id", "nombre", unidad.departamentosId);
            return View(unidad);
        }

        // GET: Unidades/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            unidad unidad = await db.unidad.FindAsync(id);
            if (unidad == null)
            {
                return HttpNotFound();
            }
            return View(unidad);
        }

        // POST: Unidades/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            unidad unidad = await db.unidad.FindAsync(id);
            db.unidad.Remove(unidad);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
