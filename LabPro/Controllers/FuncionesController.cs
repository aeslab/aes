﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LabPro.Models;
using LabPro.Servicios;

namespace LabPro.Controllers
{
    public class FuncionesController : Controller
    {
        private DB_EPPEntities db = new DB_EPPEntities();

        // GET: Funciones
        public async Task<ActionResult> Index()
        {
            var query = (from f in db.Funciones select f).Include(x => x.FuncionesRoles);
            List<Funciones> funciones = new List<Funciones>();
            var roles = await query.ToListAsync();
             foreach( var d in roles)
            {
                var funciones_roles = d.FuncionesRoles.Select(f => f.id_rol).ToList();
                var nombres_roles = db.AspNetRoles.Where(y => funciones_roles.Contains(y.Id)).Select(y => y.Name).ToArray();
                funciones.Add(new Funciones() {
                    Id = d.Id,
                    funcion = d.funcion,
                    nombre_mostrar = d.nombre_mostrar,
                    Roles = string.Join(",",nombres_roles)
                });
            }
            return View(funciones);
        }

        // GET: Funciones/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funciones funciones = await db.Funciones.FindAsync(id);
            if (funciones == null)
            {
                return HttpNotFound();
            }
            return View(funciones);
        }

        // GET: Funciones/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Funciones/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,funcion,nombre_mostrar")] Funciones funciones)
        {
            if (ModelState.IsValid)
            {
                db.Funciones.Add(funciones);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(funciones);
        }

        // GET: Funciones/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funciones funciones = await db.Funciones.FindAsync(id);
            if (funciones == null)
            {
                return HttpNotFound();
            }
            var funcionesRoles = db.FuncionesRoles.Where(f => f.id_funcion == funciones.Id).ToList();
            ViewBag.roles = db.AspNetRoles.ToList();
            MultiSelectList m = new MultiSelectList(ViewBag.roles, "Id", "Name", funcionesRoles.Select(r => r.id_rol).ToList());
            ViewBag.roles = m;

            return View(funciones);
        }

        // POST: Funciones/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,funcion,nombre_mostrar")] Funciones funciones, string[] roles)
        {
            if (ModelState.IsValid)
            {

                db.Entry(funciones).State = EntityState.Modified;
                db.FuncionesRoles.RemoveRange(db.FuncionesRoles.Where(f => f.id_funcion == funciones.Id));
                if (roles != null)
                    foreach (var r in roles)
                    {
                        db.FuncionesRoles.Add(new FuncionesRoles() { id_funcion = funciones.Id, id_rol = r });
                    }
                await db.SaveChangesAsync();

                RolesFuncionService.ActualizarDiccionario();
                return RedirectToAction("Index");

            }
            return View(funciones);
        }

        // GET: Funciones/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funciones funciones = await db.Funciones.FindAsync(id);
            if (funciones == null)
            {
                return HttpNotFound();
            }
            return View(funciones);
        }

        // POST: Funciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Funciones funciones = await db.Funciones.FindAsync(id);
            db.Funciones.Remove(funciones);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
