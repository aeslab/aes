﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LabPro.Models;

namespace LabPro.Controllers
{
    public class empresasController : Controller
    {
        private DB_EPPEntities db = new DB_EPPEntities();

        // GET: empresas
        public async Task<ActionResult> Index()
        {
            var empresas = db.empresas.Include(e => e.Paises);
            return View(await empresas.ToListAsync());
        }

        // GET: empresas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            empresas empresas = await db.empresas.FindAsync(id);
            if (empresas == null)
            {
                return HttpNotFound();
            }
            return View(empresas);
        }

        // GET: empresas/Create
        public ActionResult Create()
        {
            ViewBag.PaisesId = new SelectList(db.Paises, "Id", "nombre");
            return View();
        }

        // POST: empresas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,nombre,nomenclatura,PaisesId")] empresas empresas)
        {
            if (ModelState.IsValid)
            {
                db.empresas.Add(empresas);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.PaisesId = new SelectList(db.Paises, "Id", "nombre", empresas.PaisesId);
            return View(empresas);
        }

        // GET: empresas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            empresas empresas = await db.empresas.FindAsync(id);
            if (empresas == null)
            {
                return HttpNotFound();
            }
            ViewBag.PaisesId = new SelectList(db.Paises, "Id", "nombre", empresas.PaisesId);
            return View(empresas);
        }

        // POST: empresas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,nombre,nomenclatura,PaisesId")] empresas empresas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(empresas).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.PaisesId = new SelectList(db.Paises, "Id", "nombre", empresas.PaisesId);
            return View(empresas);
        }

        // GET: empresas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            empresas empresas = await db.empresas.FindAsync(id);
            if (empresas == null)
            {
                return HttpNotFound();
            }
            return View(empresas);
        }

        // POST: empresas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            empresas empresas = await db.empresas.FindAsync(id);
            db.empresas.Remove(empresas);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
