﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LabPro.Models;
using DataTables.Mvc;

namespace LabPro.Controllers
{
    public class PruebasPCBBarrilController : Controller
    {
        private DB_EPPEntities db = new DB_EPPEntities();

        // GET: PruebasPCBBarril
        public async Task<ActionResult> Index()
        {
            var pruebas_pcb_barril = db.pruebas_pcb_barril.Include(p => p.empresas).OrderByDescending(m => m.Id);
            return View(await pruebas_pcb_barril.ToListAsync());
        }

        // GET: PruebasPCBBarril/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pruebas_pcb_barril pruebas_pcb_barril = await db.pruebas_pcb_barril.FindAsync(id);
            if (pruebas_pcb_barril == null)
            {
                return HttpNotFound();
            }
            return View(pruebas_pcb_barril);
        }

        // GET: PruebasPCBBarril/Create
        public ActionResult Create()
        {
            ViewBag.id_empresa = new SelectList(db.empresas, "Id", "nombre");
            return View();
        }

        // POST: PruebasPCBBarril/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,codigo,nombre,ppm,id_empresa,fecha_prueba")] pruebas_pcb_barril pruebas_pcb_barril)
        {
            if (ModelState.IsValid)
            {

                var empresa = await db.empresas.FindAsync(pruebas_pcb_barril.id_empresa);
                var correlativo = db.correlativos_empresa.Where(ce => ce.id_empresa == empresa.Id).First();
                pruebas_pcb_barril.codigo = empresa.nomenclatura + pruebas_pcb_barril.fecha_prueba.Value.Year.ToString().Substring(2, 2) + correlativo.correlativo;
                correlativo.correlativo++;

                db.pruebas_pcb_barril.Add(pruebas_pcb_barril);
                await db.SaveChangesAsync();

                return RedirectToAction("Index");
            }

            ViewBag.id_empresa = new SelectList(db.empresas, "Id", "nombre", pruebas_pcb_barril.id_empresa);
            return View(pruebas_pcb_barril);
        }

        // GET: PruebasPCBBarril/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pruebas_pcb_barril pruebas_pcb_barril = await db.pruebas_pcb_barril.FindAsync(id);
            if (pruebas_pcb_barril == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_empresa = new SelectList(db.empresas, "Id", "nombre", pruebas_pcb_barril.id_empresa);
            return View(pruebas_pcb_barril);
        }

        // POST: PruebasPCBBarril/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,codigo,nombre,ppm,id_empresa,fecha_prueba")] pruebas_pcb_barril pruebas_pcb_barril)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pruebas_pcb_barril).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.id_empresa = new SelectList(db.empresas, "Id", "nombre", pruebas_pcb_barril.id_empresa);
            return View(pruebas_pcb_barril);
        }

        // GET: PruebasPCBBarril/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pruebas_pcb_barril pruebas_pcb_barril = await db.pruebas_pcb_barril.FindAsync(id);
            if (pruebas_pcb_barril == null)
            {
                return HttpNotFound();
            }
            return View(pruebas_pcb_barril);
        }

        // POST: PruebasPCBBarril/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            pruebas_pcb_barril pruebas_pcb_barril = await db.pruebas_pcb_barril.FindAsync(id);
            db.pruebas_pcb_barril.Remove(pruebas_pcb_barril);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        public async Task<JsonResult> CodigoPCBBarril(int? id)
        {
            if (id == null)
                return Json(new { codigo = "" }, JsonRequestBehavior.AllowGet);

            var empresa = await db.empresas.FindAsync(id);
            var correlativo = db.correlativos_empresa.Where(ce => ce.id_empresa == empresa.Id).First();
            var codigo = empresa.nomenclatura + DateTime.Now.Year.ToString().Substring(2, 2) + correlativo.correlativo;

            return Json(new { codigo }, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> DatosPCBBarril([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {

            var query = db.pruebas_pcb_barril.Select(x => x);
            var column = requestModel.Columns.Where(c => c.IsOrdered == true).First();
            var desc = column.SortDirection == Column.OrderDirection.Descendant;
            switch (column.OrderNumber)
            {
                case 0:
                    query = desc ? query.OrderByDescending(q => q.codigo) : query.OrderBy(q => q.codigo);
                    break;
                case 1:
                    query = desc ? query.OrderByDescending(q => q.nombre) : query.OrderBy(q => q.nombre);
                    break;
                case 3:
                    query = desc ? query.OrderByDescending(q => q.fecha_prueba) : query.OrderBy(q => q.fecha_prueba);
                    break;
            }
            if (!string.IsNullOrEmpty(requestModel.Search.Value))
            {
                query = query.Where(
                    q => q.codigo.Contains(requestModel.Search.Value) ||
                         (
                         (q.fecha_prueba.Value.Day.ToString().Length == 2 ? q.fecha_prueba.Value.Day.ToString() : "0" + q.fecha_prueba.Value.Day)+ "/" +
                         (q.fecha_prueba.Value.Month.ToString().Length == 2 ? q.fecha_prueba.Value.Month.ToString() : "0" + q.fecha_prueba.Value.Month) + "/" +
                         q.fecha_prueba.Value.Year).Contains(requestModel.Search.Value)
                    );
            }
            int total_registros = query.Count();

            query = query.Skip(requestModel.Start).Take(requestModel.Length);
            var datos_lista = new List<object[]>();
            await query.Include(q => q.empresas).ForEachAsync(d =>
            {
                datos_lista.Add(
                    new object[]
                    {
                        d.codigo,d.nombre,d.ppm,d.fecha_prueba.Value.ToString("dd/MM/yyyy"),d.empresas.nombre
                    }
                    );
            });
            return Json(new { recordsFiltered = total_registros, recordsTotal = total_registros, data = datos_lista }, JsonRequestBehavior.AllowGet);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
