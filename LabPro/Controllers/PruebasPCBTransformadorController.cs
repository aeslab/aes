﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LabPro.Models;
using DataTables.Mvc;

namespace LabPro.Controllers
{
    public class PruebasPCBTransformadorController : Controller
    {
        private DB_EPPEntities db = new DB_EPPEntities();

        // GET: PruebasPCBTransformador
        public async Task<ActionResult> Index()
        {
            var pruebas_pcb_transformador = db.pruebas_pcb_transformador.Include(p => p.empresas).Include(p => p.marcas_equipo);
            return View(await pruebas_pcb_transformador.ToListAsync());
        }

        // GET: PruebasPCBTransformador/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pruebas_pcb_transformador pruebas_pcb_transformador = await db.pruebas_pcb_transformador.FindAsync(id);
            if (pruebas_pcb_transformador == null)
            {
                return HttpNotFound();
            }
            return View(pruebas_pcb_transformador);
        }

        // GET: PruebasPCBTransformador/Create
        public ActionResult Create()
        {
            ViewBag.id_empresa = new SelectList(db.empresas, "Id", "nombre");
            ViewBag.id_marca = new SelectList(db.marcas_equipo, "Id", "nombre");
            return View(new pruebas_pcb_transformador() { fecha_prueba = DateTime.Now.Date });
        }

        // POST: PruebasPCBTransformador/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,serie,id_marca,fecha_fabricacion,kva,voltaje,peso,gal_aceite,ppm,id_empresa,fecha_prueba")] pruebas_pcb_transformador pruebas_pcb_transformador)
        {
            if (ModelState.IsValid)
            {
                db.pruebas_pcb_transformador.Add(pruebas_pcb_transformador);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.id_empresa = new SelectList(db.empresas, "Id", "nombre", pruebas_pcb_transformador.id_empresa);
            ViewBag.id_marca = new SelectList(db.marcas_equipo, "Id", "nombre", pruebas_pcb_transformador.id_marca);
            return View(pruebas_pcb_transformador);
        }

        // GET: PruebasPCBTransformador/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pruebas_pcb_transformador pruebas_pcb_transformador = await db.pruebas_pcb_transformador.FindAsync(id);
            if (pruebas_pcb_transformador == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_empresa = new SelectList(db.empresas, "Id", "nombre", pruebas_pcb_transformador.id_empresa);
            ViewBag.id_marca = new SelectList(db.marcas_equipo, "Id", "nombre", pruebas_pcb_transformador.id_marca);
            return View(pruebas_pcb_transformador);
        }

        // POST: PruebasPCBTransformador/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,serie,id_marca,fecha_fabricacion,kva,voltaje,peso,gal_aceite,ppm,id_empresa,fecha_prueba")] pruebas_pcb_transformador pruebas_pcb_transformador)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pruebas_pcb_transformador).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.id_empresa = new SelectList(db.empresas, "Id", "nombre", pruebas_pcb_transformador.id_empresa);
            ViewBag.id_marca = new SelectList(db.marcas_equipo, "Id", "nombre", pruebas_pcb_transformador.id_marca);
            return View(pruebas_pcb_transformador);
        }

        // GET: PruebasPCBTransformador/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pruebas_pcb_transformador pruebas_pcb_transformador = await db.pruebas_pcb_transformador.FindAsync(id);
            if (pruebas_pcb_transformador == null)
            {
                return HttpNotFound();
            }
            return View(pruebas_pcb_transformador);
        }

        // POST: PruebasPCBTransformador/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            pruebas_pcb_transformador pruebas_pcb_transformador = await db.pruebas_pcb_transformador.FindAsync(id);
            db.pruebas_pcb_transformador.Remove(pruebas_pcb_transformador);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public async Task<JsonResult> DatosPCBTransformador([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {

            var query = db.pruebas_pcb_transformador.Select(x => x);
            var column = requestModel.Columns.Where(c => c.IsOrdered == true).First();
            var desc = column.SortDirection == Column.OrderDirection.Descendant;
            switch (column.OrderNumber)
            {
                case 0:
                    query = desc ? query.OrderByDescending(q => q.serie) : query.OrderBy(q => q.serie);
                    break;
                case 1:
                    query = desc ? query.OrderByDescending(q => q.fecha_fabricacion) : query.OrderBy(q => q.fecha_fabricacion);
                    break;
                case 8:
                    query = desc ? query.OrderByDescending(q => q.id_empresa) : query.OrderBy(q => q.id_empresa);
                    break;
            }
            if (!string.IsNullOrEmpty(requestModel.Search.Value))
            {
                query = query.Where(
                    q => q.serie.Contains(requestModel.Search.Value) ||
                         (
                         (q.fecha_prueba.Value.Day.ToString().Length == 2 ? q.fecha_prueba.Value.Day.ToString() : "0" + q.fecha_prueba.Value.Day) + "/" +
                         (q.fecha_prueba.Value.Month.ToString().Length == 2 ? q.fecha_prueba.Value.Month.ToString() : "0" + q.fecha_prueba.Value.Month) + "/" +
                         q.fecha_prueba.Value.Year).Contains(requestModel.Search.Value)
                    );
            }
            int total_registros = query.Count();

            query = query.Skip(requestModel.Start).Take(requestModel.Length);
            var datos_lista = new List<object[]>();
            await query.Include(q => q.empresas).Include(q=>q.marcas_equipo).ForEachAsync(d =>
            {
                datos_lista.Add(
                    new object[]
                    {
                        d.serie,d.fecha_fabricacion,d.kva,d.voltaje,d.peso,d.gal_aceite,d.ppm,d.fecha_prueba.Value.ToString("dd/MM/yyyy"),d.empresas.nombre,d.marcas_equipo.nombre
                    }
                    );
            });
            return Json(new { recordsFiltered = total_registros, recordsTotal = total_registros, data = datos_lista }, JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
