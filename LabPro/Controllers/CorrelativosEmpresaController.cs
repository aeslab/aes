﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LabPro.Models;

namespace LabPro.Controllers
{
    public class CorrelativosEmpresaController : Controller
    {
        private DB_EPPEntities db = new DB_EPPEntities();

        // GET: CorrelativosEmpresa
        public async Task<ActionResult> Index()
        {
            var correlativos_empresa = db.correlativos_empresa.Include(c => c.empresas);
            return View(await correlativos_empresa.ToListAsync());
        }

        // GET: CorrelativosEmpresa/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            correlativos_empresa correlativos_empresa = await db.correlativos_empresa.FindAsync(id);
            if (correlativos_empresa == null)
            {
                return HttpNotFound();
            }
            return View(correlativos_empresa);
        }

        // GET: CorrelativosEmpresa/Create
        public ActionResult Create()
        {
            ViewBag.id_empresa = new SelectList(db.empresas, "Id", "nombre");
            return View();
        }

        // POST: CorrelativosEmpresa/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,id_empresa,correlativo")] correlativos_empresa correlativos_empresa)
        {
            if (ModelState.IsValid)
            {
                db.correlativos_empresa.Add(correlativos_empresa);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.id_empresa = new SelectList(db.empresas, "Id", "nombre", correlativos_empresa.id_empresa);
            return View(correlativos_empresa);
        }

        // GET: CorrelativosEmpresa/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            correlativos_empresa correlativos_empresa = await db.correlativos_empresa.FindAsync(id);
            if (correlativos_empresa == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_empresa = new SelectList(db.empresas, "Id", "nombre", correlativos_empresa.id_empresa);
            return View(correlativos_empresa);
        }

        // POST: CorrelativosEmpresa/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,id_empresa,correlativo")] correlativos_empresa correlativos_empresa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(correlativos_empresa).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.id_empresa = new SelectList(db.empresas, "Id", "nombre", correlativos_empresa.id_empresa);
            return View(correlativos_empresa);
        }

        // GET: CorrelativosEmpresa/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            correlativos_empresa correlativos_empresa = await db.correlativos_empresa.FindAsync(id);
            if (correlativos_empresa == null)
            {
                return HttpNotFound();
            }
            return View(correlativos_empresa);
        }

        // POST: CorrelativosEmpresa/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            correlativos_empresa correlativos_empresa = await db.correlativos_empresa.FindAsync(id);
            db.correlativos_empresa.Remove(correlativos_empresa);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
