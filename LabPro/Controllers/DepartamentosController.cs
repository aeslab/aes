﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LabPro.Models;
using System.Text;

namespace LabPro.Controllers
{
    public class DepartamentosController : Controller
    {
        private DB_EPPEntities db = new DB_EPPEntities();
        [HttpPost]
        public async Task<JsonResult> DepartamentosAjax(int idempresa)
        {
            var departamentos = await db.departamentos.Where(d => d.empresasId == idempresa).ToListAsync();
            StringBuilder sb = new StringBuilder();
            foreach(var dep in departamentos)
            {
                sb.Append(string.Format("<option value = '{0}'>{1}</option>",dep.Id,dep.nombre));
            }
            return Json(sb.ToString());
        }
        // GET: Departamentos
        public async Task<ActionResult> Index()
        {
            var departamentos = db.departamentos.Include(d => d.empresas);
            return View(await departamentos.ToListAsync());
        }

        // GET: Departamentos/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            departamentos departamentos = await db.departamentos.FindAsync(id);
            if (departamentos == null)
            {
                return HttpNotFound();
            }
            return View(departamentos);
        }

        // GET: Departamentos/Create
        public ActionResult Create()
        {
            ViewBag.empresasId = new SelectList(db.empresas, "Id", "nombre");
            return View();
        }

        // POST: Departamentos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,nombre,empresasId")] departamentos departamentos)
        {
            if (ModelState.IsValid)
            {
                db.departamentos.Add(departamentos);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.empresasId = new SelectList(db.empresas, "Id", "nombre", departamentos.empresasId);
            return View(departamentos);
        }

        // GET: Departamentos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            departamentos departamentos = await db.departamentos.FindAsync(id);
            if (departamentos == null)
            {
                return HttpNotFound();
            }
            ViewBag.empresasId = new SelectList(db.empresas, "Id", "nombre", departamentos.empresasId);
            return View(departamentos);
        }

        // POST: Departamentos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,nombre,empresasId")] departamentos departamentos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(departamentos).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.empresasId = new SelectList(db.empresas, "Id", "nombre", departamentos.empresasId);
            return View(departamentos);
        }

        // GET: Departamentos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            departamentos departamentos = await db.departamentos.FindAsync(id);
            if (departamentos == null)
            {
                return HttpNotFound();
            }
            return View(departamentos);
        }

        // POST: Departamentos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            departamentos departamentos = await db.departamentos.FindAsync(id);
            db.departamentos.Remove(departamentos);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
