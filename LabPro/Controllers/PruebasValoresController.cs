﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LabPro.Models;

namespace LabPro.Controllers
{
    public class PruebasValoresController : Controller
    {
        private DB_EPPEntities db = new DB_EPPEntities();

        // GET: PruebasValores
        public async Task<ActionResult> Index()
        {
            var pruebas_valores = db.pruebas_valores.Include(p => p.pruebas);
            return View(await pruebas_valores.ToListAsync());
        }

        // GET: PruebasValores/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pruebas_valores pruebas_valores = await db.pruebas_valores.FindAsync(id);
            if (pruebas_valores == null)
            {
                return HttpNotFound();
            }
            return View(pruebas_valores);
        }

        // GET: PruebasValores/Create
        public ActionResult Create()
        {
            ViewBag.pruebasId = new SelectList(db.pruebas, "Id", "nombre");
            return View();
        }

        // POST: PruebasValores/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,umbral_maximo,umbral_minimo,pruebasId,descripcion")] pruebas_valores pruebas_valores)
        {
            if (ModelState.IsValid)
            {
                db.pruebas_valores.Add(pruebas_valores);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.pruebasId = new SelectList(db.pruebas, "Id", "nombre", pruebas_valores.pruebasId);
            return View(pruebas_valores);
        }

        // GET: PruebasValores/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pruebas_valores pruebas_valores = await db.pruebas_valores.FindAsync(id);
            if (pruebas_valores == null)
            {
                return HttpNotFound();
            }
            ViewBag.pruebasId = new SelectList(db.pruebas, "Id", "nombre", pruebas_valores.pruebasId);
            return View(pruebas_valores);
        }

        // POST: PruebasValores/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,umbral_maximo,umbral_minimo,pruebasId,descripcion")] pruebas_valores pruebas_valores)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pruebas_valores).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.pruebasId = new SelectList(db.pruebas, "Id", "nombre", pruebas_valores.pruebasId);
            return View(pruebas_valores);
        }

        // GET: PruebasValores/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pruebas_valores pruebas_valores = await db.pruebas_valores.FindAsync(id);
            if (pruebas_valores == null)
            {
                return HttpNotFound();
            }
            return View(pruebas_valores);
        }

        // POST: PruebasValores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            pruebas_valores pruebas_valores = await db.pruebas_valores.FindAsync(id);
            db.pruebas_valores.Remove(pruebas_valores);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
