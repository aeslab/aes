﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LabPro.Models;

namespace LabPro.Controllers
{
    public class MarcasEquipoController : Controller
    {
        private DB_EPPEntities db = new DB_EPPEntities();

        // GET: MarcasEquipo
        public async Task<ActionResult> Index()
        {
            return View(await db.marcas_equipo.ToListAsync());
        }

        // GET: MarcasEquipo/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            marcas_equipo marcas_equipo = await db.marcas_equipo.FindAsync(id);
            if (marcas_equipo == null)
            {
                return HttpNotFound();
            }
            return View(marcas_equipo);
        }

        // GET: MarcasEquipo/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MarcasEquipo/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,nombre")] marcas_equipo marcas_equipo)
        {
            if (ModelState.IsValid)
            {
                db.marcas_equipo.Add(marcas_equipo);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(marcas_equipo);
        }

        // GET: MarcasEquipo/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            marcas_equipo marcas_equipo = await db.marcas_equipo.FindAsync(id);
            if (marcas_equipo == null)
            {
                return HttpNotFound();
            }
            return View(marcas_equipo);
        }

        // POST: MarcasEquipo/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,nombre")] marcas_equipo marcas_equipo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(marcas_equipo).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(marcas_equipo);
        }

        // GET: MarcasEquipo/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            marcas_equipo marcas_equipo = await db.marcas_equipo.FindAsync(id);
            if (marcas_equipo == null)
            {
                return HttpNotFound();
            }
            return View(marcas_equipo);
        }

        // POST: MarcasEquipo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            marcas_equipo marcas_equipo = await db.marcas_equipo.FindAsync(id);
            db.marcas_equipo.Remove(marcas_equipo);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
