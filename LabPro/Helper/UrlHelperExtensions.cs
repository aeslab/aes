﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LabPro.Helper
{
    public static class UrlHelperExtensions
    {
        public static string IsAnyTreeviewActive(this UrlHelper url, Dictionary<string, string> actions)
        {
            var controller = url.RequestContext.RouteData.Values["controller"].ToString();

            var action = url.RequestContext.RouteData.Values["action"].ToString();

            if (actions.Any(a => a.Key == (action + controller) && a.Value == controller))

            {

                return "active";

            }



            return "";
        }
    }
}