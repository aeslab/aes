﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LabPro.Helper
{
    
    public static class LanguageHelper
    {
        public static MvcHtmlString LangSwitcher(this UrlHelper url, string Name, RouteData routeData, string lang)
        {
            var liTagBuilder = new TagBuilder("li");
            var aTagBuilder = new TagBuilder("a");
            var iTagBuilder = new TagBuilder("i");
            var pTagBuilder = new TagBuilder("p");
            iTagBuilder.MergeAttribute("class", "fa fa-flag-o text-blue");
            aTagBuilder.MergeAttribute("class", "text-blue");
            var routeValueDictionary = new RouteValueDictionary(routeData.Values);
            if (routeValueDictionary.ContainsKey("lang"))
            {
                if (routeData.Values["lang"] as string == lang)
                {
                    liTagBuilder.AddCssClass("active");
                    pTagBuilder.InnerHtml = iTagBuilder.ToString()+" "+Name;
                    //liTagBuilder.InnerHtml = iTagBuilder.ToString();
                    
                }
                else
                {
                    pTagBuilder.InnerHtml = Name;
                    routeValueDictionary["lang"] = lang;
                }
            }
            aTagBuilder.MergeAttribute("href", url.RouteUrl(routeValueDictionary));
            
            //aTagBuilder.SetInnerText(Name);
            aTagBuilder.InnerHtml = pTagBuilder.ToString();
            liTagBuilder.InnerHtml = aTagBuilder.ToString();
            return new MvcHtmlString(liTagBuilder.ToString());
        }
       
    }
}