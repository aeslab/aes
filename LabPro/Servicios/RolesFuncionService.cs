﻿using LabPro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LabPro.Servicios
{
    public class RolesFuncionService
    {
        private RolesFuncionService() {}
        private static DB_EPPEntities db = new DB_EPPEntities();
        private static Dictionary<string, string[]> funciones_roles;
        public static string[] Roles(string nombreFuncion)
        {
            funciones_roles.TryGetValue(nombreFuncion, out string[] roles);
            return roles;
        }
        public static void ActualizarDiccionario()
        {
            funciones_roles = (from rol in db.AspNetRoles
                               join funcionesRoles in db.FuncionesRoles on rol.Id equals funcionesRoles.id_rol
                               join funciones in db.Funciones on funcionesRoles.id_funcion equals funciones.Id
                               group rol.Name by funciones.funcion into FuncionRoles
                               select FuncionRoles).ToDictionary(d => d.Key, d => d.ToArray());
        }
        
    }
}