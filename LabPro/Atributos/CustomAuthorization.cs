﻿using LabPro.Models;
using LabPro.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace System.Web.Mvc
{
    public static class CustomUser
    {
        public static bool HasAccess(this IPrincipal principal, string opcion)
        {
            var roles = RolesFuncionService.Roles(opcion);
            if (roles != null)
                foreach (var rol in roles)
                {
                    if (HttpContext.Current.User.IsInRole(rol))
                    {
                        return true;
                    }
                }
            return false;
        }
        
    }
    public class CustomAuthorization : AuthorizeAttribute
    {
        public string OPCION { get; set; }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return false;

            var roles_funcion = RolesFuncionService.Roles(OPCION);
            if(roles_funcion != null)
            foreach(var rol in roles_funcion)
            {
                if(HttpContext.Current.User.IsInRole(rol))
                {
                    return true;
                }
            }
            return false; // no autorizado
        }

    }
}