﻿using LabPro.App_LocalResources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LabPro.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Código")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "¿Recordar este explorador?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "AcountLogin_Email",ResourceType =typeof(GlobalResource))]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "AcountLogin_Password",ResourceType =typeof(GlobalResource))]
        public string Password { get; set; }

        [Display(Name = "AcountLogin_RememberMe",ResourceType =typeof(GlobalResource))]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "AcountRegister_Email", ResourceType =typeof(GlobalResource))]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceType =typeof(GlobalResource),ErrorMessageResourceName = "AcountRegister_ErrorPassword", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "AcountRegister_Password",ResourceType =typeof(GlobalResource))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "AcountRegister_ConfirmPassword",ResourceType =typeof(GlobalResource))]
        [Compare("Password", ErrorMessageResourceName = "AcountRegister_ErrorConfirmPassword", ErrorMessageResourceType = typeof(GlobalResource))]
        public string ConfirmPassword { get; set; }

        public int Id_empleado { get; set; }
        public List<empleados> Empleados { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "El número de caracteres de {0} debe ser al menos {2}.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar contraseña")]
        [Compare("Password", ErrorMessage = "La contraseña y la contraseña de confirmación no coinciden.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }
    }
}
