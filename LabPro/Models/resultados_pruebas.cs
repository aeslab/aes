//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LabPro.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class resultados_pruebas
    {
        public int Id { get; set; }
        public int pruebas_equiposId { get; set; }
        public int pruebas_valoresId { get; set; }
        public string valor_obtenido { get; set; }
    
        public virtual pruebas_equipos pruebas_equipos { get; set; }
        public virtual pruebas_valores pruebas_valores { get; set; }
    }
}
