//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LabPro.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class equipos_clientes
    {
        public int Id { get; set; }
        public int equiposId { get; set; }
        public int clientesId { get; set; }
    
        public virtual clientes clientes { get; set; }
        public virtual equipos equipos { get; set; }
    }
}
