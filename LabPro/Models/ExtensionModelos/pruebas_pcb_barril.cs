﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LabPro.Models
{
    public class pruebas_pcb_barril_meta
    {
        [Required]
        public Nullable<double> ppm { get; set; }
        [Required]
        public Nullable<int> id_empresa { get; set; }
        [Required]
        public Nullable<System.DateTime> fecha_prueba { get; set; }
    }

    [MetadataType(typeof(pruebas_pcb_barril_meta))]
    public partial class pruebas_pcb_barril
    {}
}